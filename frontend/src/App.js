import Main from "./components/main";
import "bootstrap/dist/css/bootstrap.min.css";
import "../src/CSS/App.css";

import { library } from '@fortawesome/fontawesome-svg-core';
import { faB } from "@fortawesome/free-solid-svg-icons";
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons';

library.add(faB, faCheckSquare, faCoffee)
function App() {
  return (
    <div>
        <Main/>
    </div>
  );
}

export default App;
