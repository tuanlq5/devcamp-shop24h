import React, { useEffect, useRef, useState } from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import logo from "../assets/images/logo.png";
import axios from "axios";
import {Container, 
    Row, 
    Col, 
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledCarousel, Input, Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';

function Main(args) {
    //state for product
    const [product, setProduct] = useState([]);
    //navbar
    const [isOpen, setIsOpen] = useState(false);
    //state for countdown clock
    const [timeDays, setTimerDays] = useState("00");
    const [timeHours, setTimerHours] = useState("00");
    const [timeMinutes, setTimerMinutes] = useState("00");
    const [timeSeconds, setTimerSeconds] = useState("00");
    const toggle = () => setIsOpen(!isOpen);
    //AXIOS CAll
    const axiosCall = async(url, body) => {
        const response = await axios(url, body);
        return response.data;
    }
    let interval =  useRef();
    // Function set countdown for the deal
    const startTimer = () => {
        const countDownDate = new Date(`February 25 2023 00:00:00`).getTime();

        interval = setInterval(()=> {
            const now = new Date().getTime();
            const distance = countDownDate - now;
            
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
            const minutes = Math.floor(distance % (1000 * 60 * 60 ) / (1000 * 60));
            const seconds = Math.floor(distance % (1000 * 60  ) / 1000 );

            if(distance < 0 ){
                //stop our time
                clearInterval(interval.current)
            } else {
                //update time
                setTimerDays(days);
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }
        }, 1000);
    }
    
    useEffect(()=> {
       // axios get all product
        const getAllProduct = () => {
            axiosCall("/product")
                .then((data) => {
                    console.log(data.data);
                    setProduct(data.data);
                })
                .catch((error) => {
                    console.log(error.message)
                })
        }
        getAllProduct();
        
    }, [])
    useEffect(()=>{
        startTimer();
        
    })
    return( 
     <div>
        <HelmetProvider>
            <div>
            <Helmet>
                <title>ADIDAS</title>
                <meta name="viewport" content="width=device-width,initial-scale=1"/>
            </Helmet>
                <Container  className='bg-img'>
                {/* Navbar */}
                        <Navbar {...args} fixed="top" expand='md'  dark= {true}  className="container" id='nav1'>
                                <NavbarBrand href="/">
                                    <img src={logo} alt="logo" className='logo'/>
                                </NavbarBrand>
                                <NavbarToggler onClick={toggle} />
                                <Collapse isOpen={isOpen}  navbar  >
                                <Nav  navbar  className='w-100 hearder-link-position' horizontal='end'>
                                    <NavItem>
                                        <NavLink href="/" className='headerLink mx-3'><span>Men</span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/" className='headerLink mx-3'><span>Woman</span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/" className='headerLink mx-3'><span>Child</span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"  className='headerLink mx-3'><span>Collection</span></NavLink>
                                    </NavItem>
                                </Nav>
                                <Nav  navbar   className='w-50 ' horizontal='end'>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faFacebook} size="lg"/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faInstagram} size="lg"/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faTwitter} size="lg"/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faShoppingCart} size="lg"/></span></NavLink>
                                    </NavItem>
                                </Nav>
                                </Collapse>
                        </Navbar>
                {/* Carousel */}
                    <div className='carousel'>
                        <UncontrolledCarousel  interval={false}
                            items={[
                                {
                                altText: '',
                                key: 1,
                                src: require("../assets/images/carousel/1.jpg"),
                                caption:""
                                },
                                {
                                altText: '',
                                key: 2,
                                src: require("../assets/images/carousel/2.jpg"),
                                caption:""
                                },              
                            ]}
                            />
                    </div>
                    {/* Collection */}
                    <div className='Collection'>
                        <Container>
                            <Row >
                                <Col lg={12} >
                                    <div className='banner__item d-flex justify-content-center'>
                                        <img src={require("../assets/images/collection2022/nmd.webp")} className="collection-images" alt="nmd" />
                                        <div className='banner-collection-nmd'>
                                            <h2 className='mx-3 mt-2 mb-3' >Nmd Collection 2022</h2>
                                            <a href='/' className='Collection-shop mx-3 my-4'>Shop now</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={5} >
                                    <div className='collection-ultraboost banner__item d-flex justify-content-center'>
                                        <img src={require("../assets/images/collection2022/ultraboost.jpg")}  className="collection-images" alt="ultaboost" />
                                        <div className='banner-collection-ultraboost' style={{float: "right"}}>
                                            <h2  className='mx-3 mt-2 mb-4'>Ultraboost Collection 2022</h2>
                                            <a href='/' className='Collection-shop mx-3 my-4'>Shop now</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={7}>
                                <div className='banner__item StanSmith-collection'>
                                        <img src={require("../assets/images/collection2022/StanSmith.png")}  className=" StanSmith-images" alt="ultaboost" />
                                        <div className='banner-collection-stansmith' style={{float: "right"}}>
                                            <h2  className='mx-3 mt-2 mb-4'>Stan Smith Are Back 2022</h2>
                                            <a href='/' className='Collection-shop mx-3 my-4'>Shop now</a>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    {/* Products */}
                    <div>
                        <Container className='Best-Seller'>
                            <Row>
                                <Col lg={12} className="text-center">
                                    <a href="/" className='catagories-link'>
                                        Products
                                    </a>
                                </Col>
                                <Col lg={12} className="mt-3">
                                    <Row>
                                        {product && product.map((items, key) => {
                                            return (<Col lg={3} className="box mt-3 text-center">
                                                        <div className='card'>
                                                            <a href="/"><img className='selling-images' alt='123' key={items._id} src={items.imageURL}/></a>
                                                            <p className='mt-2' key={items.type} >{`Name: ${items.name}`}</p>
                                                            <p key={items.createAt}>{`Price: ${items.buyPrice}`}</p>
                                                        </div>
                                                    </Col>
                                                    )
                                                }
                                            )
                                        }
                                    </Row> 
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    </Container>
                    {/* Deal of the week */}
                    <div>
                        <Container fluid className='Weekly-deal'>
                            <Row className='deal-box'>
                                <Col lg={6} className="deal-banner">
                                    <img className='deal-image' alt={"deal"} src={require("../assets/images/Deal-Shoes.png")}/>
                                </Col>
                                <Col lg={6} className=" text-center Deal-background">
                                    <p className='text-light'>DEAL OF THE WEEK</p>
                                    <h5 className='text-light'>ULTRA BOOST came back with the biggest deal of the month</h5>
                                    <div className='clock'>
                                        <p className='text-light'>{timeDays}</p>
                                        <p className='ms-1'>: </p> 
                                    
                                        <p className='text-light ms-1'>{timeHours}</p>
                                        <p className='ms-1'>: </p> 
                                    
                                        <p className='text-light ms-1'>{timeMinutes}</p>
                                        <p className='ms-1'>: </p>
                                    
                                        <p className='text-light ms-1'>{timeSeconds}</p>
                                    </div>
                                    <Button className='button-deal'>Shop Now</Button>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div>
                        <Container>
                            <Row className='advertisement'>
                                <Col sm={12}>
                                    <p style={{fontSize:"30px"}}>KEEP MOVING</p>
                                </Col>
                                <Col sm={7}>
                                    <div className='advertise-image'>
                                           <img src={require('../assets/images/advertisement/1.jpg')} alt="advertise" className='item'/> 
                                           <img src={require('../assets/images/advertisement/2.jpg')} alt="advertise" className='item'/> 
                                           <img src={require('../assets/images/advertisement/3.jpg')} alt="advertise" className='item'/> 
                                           <img src={require('../assets/images/advertisement/4.jpg')} alt="advertise" className='item'/> 
                                           <img src={require('../assets/images/advertisement/5.jpg')} alt="advertise" className='item'/> 
                                           <img src={require('../assets/images/advertisement/6.jpg')} alt="advertise" className='item'/> 
                                    </div> 
                                </Col>
                                <Col sm={5} className="Instagram">
                                    <h4>Instagram</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam culpa ducimus autem ipsum voluptate magnam nisi, sequi eligendi consequuntur assumenda quibusdam temporibus fugit corrupti aut at quas nam, eum consequatur.</p>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    {/* History */}
                    <div>
                        <Container fluid className='Background-history'>
                            <Container>
                                <Row>
                                    <Col lg={6} className="my-5">
                                        <h1>
                                        STORIES, STYLES AND SPORTSWEAR AT ADIDAS, SINCE 1949 
                                        </h1>
                                        <p>
                                        Sport keeps us fit. Keeps you mindful. Brings us together. Through sport we have the power to change lives. Whether it is through stories of inspiring athletes. Helping you to get up and get moving. Sportswear featuring the latest technologies, to up your performance. Beat your PB.adidas offers a home to the runner, the basketball player, the soccer kid, the fitness enthusiast. The weekend hiker that loves to escape the city. The yoga teacher that spreads the moves. The 3-Stripes are seen in the music scene. On stage, at festivals. Our sports clothing keeps you focused before that whistle blows. During the race. And at the finish lines. We’re here to supportcreators. Improve their game. Their lives. And change the world. 
                                        </p><br/>
                                        <p>
                                        adidas is about more than sportswear and workout clothes. We partner with the best in the industry to co-create. This way we offer our fans the sports apparel and style that match their athletic needs, while keeping sustainability in mind. We’re here to support creators. Improve their game. Create change. And we think about the impact we have on our world.
                                        </p>
                                    </Col>
                                    <Col lg={6} className="my-5">
                                        <h1>
                                        WORKOUT CLOTHES, FOR ANY SPORT
                                        </h1>
                                        <p>
                                        adidas designs for and with athletes of all kinds. Creators, who love to change the game. Challenge conventions. Break the rules and define new ones. Then break them again. We supply teams and individuals with athletic clothing pre-match. To stay focussed. We design sports apparel that get you to the finish line. To win the match. We support women, with bras and tights made for purpose. From low to high support. Maximum comfort. We design, innovate and itterate. Testing new technologies in action. On the pitch, the tracks, the court, the pool. Retro workout clothes inspire new streetwear essentials. Like NMD, Ozweego and our Firebird tracksuits. Classic sports models are brought back to life. Like Stan Smith. And Superstar. Now seen on the streets and the stages.
                                        </p><br/>
                                        <p>
                                        Through our collections we blur the borders between high fashion and high performance. Like our adidas by Stella McCartney athletic clothing collection – designed to look the part inside and outside of the gym. Or some of our adidas Originals lifestyle pieces, that can be worn as sportswear too. Our lives are constantly changing. Becoming more and more versatile. And adidas designs with that in mind.
                                        </p>
                                    </Col>
                                </Row>
                            </Container>
                        </Container>
                    </div>
                    {/* footer */}
                    <Container fluid>
                        <footer className="bg-gray-dark">
                            <Row className='d-flex justify-content-center footer-box'>
                                <Col lg = {4} className="ms-2 Contact" >
                                    <img src={require("../assets/images/adidas.png")} style={{height:"100px"}} alt="adidas" className="ms-5 footer-logo" />
                                    <h5 >Contacts</h5>
                                    <dl>
                                    <dt style={{fontWeight:"bold"}}>Address:</dt>
                                    <dd> ThuDuc District, SaiGon, VietNam</dd>
                                    </dl>
                                    <dl>
                                    <dt style={{fontWeight:"bold"}}>Email:</dt>
                                    <dd>
                                        <a href="/" className="a-footer">info@example.com</a>
                                    </dd>
                                    </dl>
                                    <dl>
                                    <dt style={{fontWeight:"bold"}}>phones:</dt>
                                    <dd>
                                        <a href="/" className="a-footer">+91 99999999 </a><span>or</span> <a href="/" className="a-footer">+91 11111111</a>
                                    </dd>
                                    </dl>
                                </Col>
                                <Col lg = {5} className="mt-5 pt-2 footer-link">
                                    <Row>
                                        <Col lg={6}>
                                            <h3>More About Us</h3>
                                            <ul className="nav-list">
                                            <li>
                                                <a className="a-footer links" id="links-about" href="/">
                                                    About
                                                </a>
                                            </li>
                                            </ul>
                                            <ul className="nav-list">
                                                <li>
                                                    <a className="a-footer links" id="links-project" href="/">Projects</a>
                                                </li>
                                            </ul>
                                            <ul className="nav-list">
                                                <li>
                                                    <a className="a-footer links" id="links-blog" href="/">Blog</a>
                                                </li>
                                            </ul>
                                            <ul className="nav-list">
                                                <li>
                                                    <a className="a-footer links" id="links-contact" href="/">Contacts</a>
                                                </li>
                                            </ul>
                                            <ul className="nav-list">
                                                <li>
                                                    <a className="a-footer links" id="links-pricing" href="/">Pricing</a>
                                                </li>
                                            </ul>
                                        </Col>
                                        <Col lg={6}>
                                            <ul className='nav-list'>
                                                <li>
                                                    <h3>New Letter</h3>
                                                </li>
                                            </ul>
                                            <ul className='nav-list'>
                                                <li>
                                                    <p>Be the first to know about new arival, look books, sales & promos!</p>
                                                </li>
                                                <Input className='Form-control input-footer' placeholder='@ your email ....'/>
                                            </ul>
                                        </Col>
                                     </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="d-flex align-items-center justify-content-center">
                                    <p>Devcamp IronHack Copyright © 2022 All rights reserved</p> 
                                </Col>
                            </Row>
                        </footer>
                    </Container>
                    
            </div>
        </HelmetProvider>
    </div>
  
    )
}

export default Main