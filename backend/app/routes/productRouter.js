// khai báo thư viện express js
const { response } = require("express");
const express = require("express");

//khai báo router app
const router = express.Router();

//import product controller
const productController = require("../controller/productController")

router.post("/product", productController.createProduct);

router.get("/product", productController.getAllProduct);

router.get("/product/:productId", productController.getProductByID);

router.put("/product/:productId", productController.updateProductById);

router.delete("/product/:productId", productController.deleteProductById)

module.exports = router;