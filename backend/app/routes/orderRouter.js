// khai báo thư viện express js
const { response } = require("express");
const express = require("express");

//khai báo router app
const router = express.Router();
//import product controller
const orderController = require("../controller/orderController");

router.post("/customer/:customerId/order", orderController.createOrderOfCustomer);

router.get("/order", orderController.getAllOrder);

router.get("/customer/:customerId/order", orderController.getAllOrderOfCustomer);

router.get("/order/:orderId", orderController.getOrderById);

router.put("/order/:orderId", orderController.updateOrderByID);

router.delete("/customer/:customerId/order/:orderId", orderController.deleteOrderByID)

module.exports = router;
