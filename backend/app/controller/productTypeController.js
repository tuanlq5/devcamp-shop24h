//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const productTypeModel = require("../models/productType");

const createProductType = (request, response) =>{
     // B1: Chuẩn bị dữ liệu
     const body = request.body;
     // console.log(body)
        //   {
        //     "bodyName": "Panadon",
        //     "bodyDescription":"Trị đau đầu" 
        //  }
 
     // B2: Validate dữ liệu
     if(!body.bodyName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
 
     // B3: Thao tác với cơ sở dữ liệu
     const newProductType = {
        name: body.bodyName,
        description:  body.bodyDescription
     }
 
     productTypeModel.create(newProductType, (error, data) => {
         if (error) {
             return response.status(500).json({
                 status: "Internal server error",
                 message: error.message
             })
         }  
         return response.status(201).json({
            status: "Create product type: Successfull",
            data: data
        })
     })
}
const getAllProductType  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    productTypeModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all productType: Successfull",
            data: data
        })
    })
}
const getProductTypeByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const productTypeID =  request.params.productTypeId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
     return response.status(400).json({
             status: "Bad request",
             message: "productTypeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     productTypeModel.findById(productTypeID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail productType  successfull",
             data: data
             })
         }
     )  
}
const updateProductTypeById =  (request, response) =>{
// bước 1: chuẩn bị dữ liệu
const productTypeID =  request.params.productTypeId;
const body = request.body;
//bước 2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productTypeID)){
    return response.status(400).json({
            status: "Bad request",
            message: "ProductTypeId không hợp lệ" 
        })
    }
    if(body.bodyName !== undefined && body.bodyName.trim() === ""){
    return response.status(400).json({
        status: "Bad request",
        message: "Name không hợp lệ"
    })
}   
    const updateProductType = {
        name: body.bodyName,
        description:  body.bodyDescription
    }
    //bước 3: gọi model tạo dữ liêu
    productTypeModel.findByIdAndUpdate(productTypeID,updateProductType,{new: true},(error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update productType  successfull",
            data: data
            })
        }
    )  
}
const deleteProductTypeById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const productTypeID = request.params.productTypeId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductType ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    productTypeModel.findByIdAndDelete(productTypeID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete product Type successfully"
        })
    })
}
module.exports = {
    createProductType,
    getAllProductType,
    getProductTypeByID,
    updateProductTypeById,
    deleteProductTypeById
}